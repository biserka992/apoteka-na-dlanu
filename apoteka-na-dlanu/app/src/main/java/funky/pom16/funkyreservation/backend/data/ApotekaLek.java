package funky.pom16.funkyreservation.backend.data;

import java.util.List;
import java.util.Vector;

/**
 * Created by j.pajic on 9/17/2016.
 */
public class ApotekaLek {
        private int cena;
        Apoteka apoteka;
        Lek lek;

        public ApotekaLek(Apoteka a, Lek l, String cena){
            this.apoteka = a;
            this.lek = l;
            this.cena = Integer.parseInt(cena);

        }

//        public ApotekaLek(String name){
//            this.name = name;
//        }
//
        public Apoteka getApoteka() {
            return apoteka;
        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
        public int getCena() {
            return cena;
        }
        public Lek getLek() {return lek;}
//
//        public void setID(int id) {
//            this.ID = id;
//        }

//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public int getPriceCat() {
//        return priceCat;
//    }
//
//    public void setPriceCat(int priceCat) {
//        this.priceCat = priceCat;
//    }
//
//    public int getRating() {
//        return rating;
//    }
//
//    public void setRating(int rating) {
//        this.rating = rating;
//    }
//
//        public Vector<Double> getLocation() {
//            return location;
//        }
//
//        public void setLocation(Vector<Double> location) {
//            this.location = location;
//        }

//    public RestaurantDetails getDetails() {
//        return details;
//    }
//
//    public void setDetails(RestaurantDetails details) {
//        this.details = details;
//    }
//    }

}
