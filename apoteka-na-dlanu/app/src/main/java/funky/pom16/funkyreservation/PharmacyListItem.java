package funky.pom16.funkyreservation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import funky.pom16.funkyreservation.backend.data.Apoteka;
import funky.pom16.funkyreservation.backend.data.ApotekaLek;

public class PharmacyListItem extends BaseAdapter {
    private Context context;
    private List<Apoteka> lista;


    public PharmacyListItem(Context context, LinkedList<Apoteka> objects) {
        super();
        this.context = context;
        this.lista = objects;
    }

    public void updateReservations(List<Apoteka> reservations){
        this.lista = reservations;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int position) {
        return this.lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return lista.get(position).getID();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(R.layout.reservation_item, parent, false);
        TextView name_view, adresa_view, radno_vreme_view, cena_view;
        name_view = (TextView) row.findViewById(R.id.restName);
        name_view.setText(this.lista.get(position).getName());
        adresa_view = (TextView) row.findViewById(R.id.date);
        adresa_view.setText(this.lista.get(position).getAdresa());
        radno_vreme_view = (TextView) row.findViewById(R.id.table);
        radno_vreme_view.setText(this.lista.get(position).getVreme());
      //  cena_view = (TextView) row.findViewById(R.id.confirmed);
       // cena_view.setText(cena_view.getText() + (this.lista.get(position).getCena() + ""));
//        if (this.reservations.get(position).getStatus()){
//            confirmed.setText("Confirmed");
//            confirmed.setTextColor(Color.GREEN);
//        } else {
//            confirmed.setText("Not Confirmed");
//            confirmed.setTextColor(Color.RED);
//        }
//*/
        return (row);

    }
}
