package funky.pom16.funkyreservation.backend.data;

import java.util.List;

public class Lek implements Comparable<Lek>{

        private String Name;
        private int ID = 0;
        private List<Apoteka> apoteke;
        private float cena;
        private String gr;

        public Lek(String name)
        {
            this.Name = name;
        }
        public Lek(String id,String name,String kolicina)
        {
            this.ID=Integer.parseInt(id);
            this.Name = name;
            this.gr=kolicina;
        }

        public String getName() {
            return Name;
        }

        public void setName(String restaurantName) {
            this.Name = restaurantName;
        }

        public int getID(){
            return this.ID;
        }

        public void setID(int newID) {
            this.ID = newID;
        }

        @Override
        public int compareTo(Lek another) {
            // if (this.getStartTime().before(another.getStartTime())) return -1;
            //else
            return 1;
        }

        public void setGr(String gr) {
            this.gr = gr;
        }

        public String getGr() {
            return this.gr;
        }

    }

