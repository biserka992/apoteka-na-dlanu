package funky.pom16.funkyreservation.backend.connection;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Vector;

import funky.pom16.funkyreservation.backend.data.Apoteka;
import funky.pom16.funkyreservation.backend.data.PasswordUtility;
import funky.pom16.funkyreservation.backend.data.Reservation;
import funky.pom16.funkyreservation.backend.data.RestaurantDetails;
import funky.pom16.funkyreservation.backend.data.Table;
import funky.pom16.funkyreservation.backend.data.User;

/**
 * Singleton class for managing mock data.
 * Is singleton to keep data between different backends consistent.
 *
 * @author Sebastian Reichl
 * @since 15.06.2016
 */
public class MockDataStore {

    private static MockDataStore instance = new MockDataStore();

    private HashMap<String, Apoteka> restaurants = new HashMap<>();
    private HashMap<String, LinkedList<Reservation>> reservations = new HashMap<>();
    private HashMap<String, User> users = new HashMap<>();
    private int reservationCounter = 0;

    public static MockDataStore getInstance() {
        return instance;
    }

    private MockDataStore() {
        Apoteka newApoteka, newApoteka2, newApoteka3,newApoteka4, newApoteka5, newApoteka6,newApoteka7, newApoteka8, newApoteka9;
        Vector<Double> coordinates = new Vector<>(2);
        Vector<Double> coordinates2 = new Vector<>(2);
        Vector<Double> coordinates3 = new Vector<>(2);
        Vector<Double> coordinates4 = new Vector<>(2);
        Vector<Double> coordinates5 = new Vector<>(2);
        Vector<Double> coordinates6 = new Vector<>(2);
        Vector<Double> coordinates7 = new Vector<>(2);
        Vector<Double> coordinates8 = new Vector<>(2);
        Vector<Double> coordinates9 = new Vector<>(2);
        RestaurantDetails details;
        Reservation slot;
        Calendar from, until;
//
//        /* First restaurant*/
//        newApoteka = new Apoteka("Medakovic Pharm");
//        newApoteka.setType("apoteka");
//        newApoteka.setPriceCat(5);
//        newApoteka.setRating(5);
//        coordinates.add(44.773914);
//        coordinates.add(20.501676);
//        details = new RestaurantDetails();
//        details.setUrl("");
//        newApoteka.setDetails(details);
//        newApoteka.setLocation(coordinates);
//
//        /* Second restaurant */
//        newApoteka2 = new Apoteka("BApoteka Beograd");
//        newApoteka2.setType("apoteka");
//        newApoteka2.setPriceCat(3);
//        newApoteka2.setRating(3);
//        coordinates2.add(44.774181);
//        coordinates2.add(20.501177);
//        details = new RestaurantDetails();
//        details.setUrl("");
//        newApoteka2.setDetails(details);
//        newApoteka2.setLocation(coordinates2);
//
//        /* Third restaurant*/
//        newApoteka3 = new Apoteka("BioPharm");
//        newApoteka3.setType("apoteka");
//        newApoteka3.setPriceCat(6);
//        newApoteka3.setRating(8);
//        coordinates3.add(44.773983);
//        coordinates3.add(20.502207);
//        details = new RestaurantDetails();
//        details.setUrl("");
//        newApoteka3.setDetails(details);
//        newApoteka3.setLocation(coordinates3);
//
//
//        newApoteka4 = new Apoteka("Benu Apoteka");
//        newApoteka4.setType("apoteka");
//        newApoteka4.setPriceCat(6);
//        newApoteka4.setRating(8);
//        coordinates4.add(44.801413);//,
//        coordinates4.add(20.484719);
//        details = new RestaurantDetails();
//        details.setUrl("https://www.benuapoteka.rs/");
//        newApoteka4.setDetails(details);
//        newApoteka4.setLocation(coordinates5);
//
//        newApoteka5 = new Apoteka("Lilly drogerie");
//        newApoteka5.setType("apoteka");
//        newApoteka5.setPriceCat(6);
//        newApoteka5.setRating(8);
//        coordinates5.add(44.799118);
//        coordinates5.add(20.484242);
//        details = new RestaurantDetails();
//        details.setUrl("https://www.lilly.rs/");
//        newApoteka5.setDetails(details);
//        newApoteka5.setLocation(coordinates5);
//
//        newApoteka6 = new Apoteka("Apoteka Lila");
//        newApoteka6.setType("apoteka");
//        newApoteka6.setPriceCat(6);
//        newApoteka6.setRating(8);
//        coordinates6.add(44.799966);//,
//        coordinates6.add(20.488109);
//        details = new RestaurantDetails();
//        details.setUrl("https://www.benuapoteka.rs/");
//        newApoteka6.setDetails(details);
//        newApoteka6.setLocation(coordinates6);
//
//        newApoteka7 = new Apoteka("Apoteka Jugovic");
//        newApoteka7.setType("apoteka");
//        newApoteka7.setPriceCat(6);
//        newApoteka7.setRating(8);
//        coordinates7.add(44.799742);//44.799742, 20.485111
//        coordinates7.add(20.485111);
//        details = new RestaurantDetails();
//        details.setUrl("https://www.lilly.rs/");
//        newApoteka7.setDetails(details);
//        newApoteka7.setLocation(coordinates7);
//
//        newApoteka8 = new Apoteka("Apoteka Zvezdara");
//        newApoteka8.setType("apoteka");
//        newApoteka8.setPriceCat(6);
//        newApoteka8.setRating(8);
//        coordinates8.add(44.803434);
//        coordinates8.add(20.481157);
//        details = new RestaurantDetails();
//        details.setUrl("https://www.benuapoteka.rs/");
//        newApoteka8.setDetails(details);
//        newApoteka8.setLocation(coordinates8);
//
//        newApoteka9 = new Apoteka("Apoteka Vuk");
//        newApoteka9.setType("apoteka");
//        newApoteka9.setPriceCat(6);
//        newApoteka9.setRating(8);
//        coordinates9.add(44.804500);
//        coordinates9.add(20.477198);
//        details = new RestaurantDetails();
//        details.setUrl("https://www.lilly.rs/"); //44.804500, 20.477198
//        newApoteka9.setDetails(details);
//        newApoteka9.setLocation(coordinates9);
//
//        /* adding generated*/
//        this.restaurants.put(newApoteka.getName(), newApoteka);
//        this.restaurants.put(newApoteka2.getName(), newApoteka2);
//        this.restaurants.put(newApoteka3.getName(), newApoteka3);
//        this.restaurants.put(newApoteka4.getName(), newApoteka4);
//        this.restaurants.put(newApoteka5.getName(), newApoteka5);
//        this.restaurants.put(newApoteka6.getName(), newApoteka6);
//        this.restaurants.put(newApoteka7.getName(), newApoteka7);
//        this.restaurants.put(newApoteka8.getName(), newApoteka8);
//        this.restaurants.put(newApoteka9.getName(), newApoteka9);

        // adding reservations
        from = Calendar.getInstance();
        from.set(Calendar.AM_PM, Calendar.PM);
        from.set(Calendar.HOUR, 1);
        from.set(Calendar.MINUTE, 0);
        from.set(Calendar.SECOND, 0);
        until = Calendar.getInstance();
        until.setTime(from.getTime());
        until.add(Calendar.HOUR, 1);
//        slot = new Reservation(from, until, new Table(1), 2, newApoteka.getName(), "someUser");
//        this.addReservation(newApoteka.getName(), slot);
//        slot = new Reservation(from, until, new Table(2), 2, newApoteka.getName(), "someUser");
//        this.addReservation(newApoteka.getName(), slot);
//        slot = new Reservation(from, until, new Table(1), 2, newApoteka3.getName(), "ArnoNyhm");
//        this.addReservation(newApoteka3.getName(), slot);
//
//        from = Calendar.getInstance();
//        from.set(Calendar.AM_PM, Calendar.PM);
//        from.set(Calendar.HOUR, 2);
//        from.set(Calendar.MINUTE, 0);
//        from.set(Calendar.SECOND, 0);
//        until = Calendar.getInstance();
//        until.setTime(from.getTime());
//        until.add(Calendar.HOUR, 1);
//        slot = new Reservation(from, until, new Table(2), 3, newApoteka2.getName(), "someUser");
//        this.addReservation(newApoteka2.getName(), slot);
//
//        from.add(Calendar.HOUR, 3);
//        until.add(Calendar.HOUR, 3);
//        slot = new Reservation(from, until, new Table(2), 3, newApoteka3.getName(), "ArnoNyhm");
//        this.addReservation(newApoteka3.getName(), slot);

        //Adding first user
        User arno = new User("ArnoNyhm", PasswordUtility.hashPassword("p"), "Arno", "Nyhm");
        users.put("ArnoNyhm", arno);

        //Adding second user
        User jan = new User("JanItor", PasswordUtility.hashPassword("fp"), "Jan", "Itor");
        users.put("JanItor", jan);
    }

    public HashMap<String, Apoteka> getRestaurantsMap() {
        return restaurants;
    }

    public HashMap<String, LinkedList<Reservation>> getReservationsMap() {
        return reservations;
    }

    /**
     * Helper function to add a reservation to the reservations map,
     * even if there is no reservations-list for selected restaurant
     * @param restaurantName The restaurants name, where the reservations is
     * @param reservation The reservation details
     */
    public void addReservation(String restaurantName, Reservation reservation){
        reservationCounter++;
        reservation.setID(reservationCounter);

        if (! this.reservations.containsKey(restaurantName)){
            LinkedList<Reservation> restaurantReservations = new LinkedList<>();
            this.reservations.put(restaurantName, restaurantReservations);
        }
        this.reservations.get(restaurantName).add(reservation);
    }

    /**
     * Deletes a given {@link Reservation} from the reservations map.
     * Does nothing, if reservation doesn't exist
     *
     * @param toDelete The reservation that is to be removed
     */
    public void deleteReservation(Reservation toDelete) {
        this.reservations.get(toDelete.getRestaurantName()).remove(toDelete);
    }

    public HashMap<String, User> getUsersMap() {
        return users;
    }

    /**
     * Puts a new {@link User} to the user list.
     * @param newUsers
     */
    public void putUser(User newUsers) {
        this.users.put(newUsers.getUsername(), newUsers);
    }
}
