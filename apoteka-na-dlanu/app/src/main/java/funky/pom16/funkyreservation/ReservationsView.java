package funky.pom16.funkyreservation;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Vector;

import cz.msebera.android.httpclient.Header;
import funky.pom16.funkyreservation.backend.FilterUtility;
import funky.pom16.funkyreservation.backend.IBackendService;
import funky.pom16.funkyreservation.backend.connection.BackendMock;
import funky.pom16.funkyreservation.backend.connection.FilterType;
import funky.pom16.funkyreservation.backend.data.Apoteka;
import funky.pom16.funkyreservation.backend.data.ApotekaLek;
import funky.pom16.funkyreservation.backend.data.Lek;

public class ReservationsView extends AppCompatActivity {
    // TODO somehow "share" backend
    private IBackendService bService;
    private LinkedList<Apoteka> apoteke=new LinkedList<Apoteka>();
    //LinkedList<Apoteka> allPharmacies;

    private ReservationListItem adapter;
    private int selectionPosition = -1;
    private Context context;
    IBackendService backend;

    String userName;
    private SearchView sv;
    private LinkedList<Lek> lekovi = new LinkedList<Lek>();
    private LinkedList<ApotekaLek> apotekeLekovi = new LinkedList<ApotekaLek>();
    private LinkedList<ApotekaLek> apotekeLekoviSvi = new LinkedList<ApotekaLek>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservations_view);
        this.context = this;
        backend = new BackendMock();
        executeAjaxRequestSviLekovi();
        executeAjaxRequestSveApotekeLek();
    //    allPharmacies = new LinkedList<>();
        userName = LoginActivity.prefs.getString("username", "");



        this.bService = new BackendMock();

       // this.pharmacies = bService.getReservations(userName);
//
//        Apoteka r1 = new Apoteka("Apoteka Beograd");
//        Apoteka r2 = new Apoteka("MediFarm");
//        Apoteka r3 = new Apoteka("BioPharm");
//        Apoteka r4 = new Apoteka("Benu Apoteka");
//        Apoteka r5 = new Apoteka("Lilly drogerie");
//        Apoteka r6 = new Apoteka("Apoteka Lila");
//        Apoteka r7 = new Apoteka("Apoteka Jugovic");
//        Apoteka r8 = new Apoteka("Apoteka Zvezdara");
//        Apoteka r9 = new Apoteka("Apoteka Vuk");

//        allPharmacies.add(r1);
//        allPharmacies.add(r2);
//        allPharmacies.add(r3);
//        allPharmacies.add(r4);
//        allPharmacies.add(r5);
//        allPharmacies.add(r6);
//        allPharmacies.add(r7);
//        allPharmacies.add(r8);
//        allPharmacies.add(r9);
//
        final ListView list_view;
        list_view = (ListView) findViewById(R.id.reservations_list);
        adapter = new ReservationListItem(getBaseContext(), apotekeLekovi);
        list_view.setAdapter(adapter);
        list_view.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        list_view.setOnItemClickListener(new ReservationClickListener(this));

        sv = (SearchView) findViewById(R.id.mapSearchView);
        CharSequence seq = getIntent().getCharSequenceExtra("search_field");
        sv.setQuery(seq, seq != null);
       sv.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

            @Override
            public boolean onQueryTextSubmit(String s) {
                apotekeLekovi.clear();
                for (Lek l : lekovi) {
                    if (l.getName().equalsIgnoreCase(s)) {
                       for(ApotekaLek apLek: apotekeLekoviSvi){
                           if(apLek.getLek().getID()==l.getID())
                               apotekeLekovi.add(apLek);
                       }

                    }
                }


//                HashMap<FilterType, Object> filter = new HashMap<>();
//                filter.put(FilterType.NAME, s);
//                LinkedList<Apoteka> pharmacies = backend.searchRestaurant(filter);
//
//                FilterUtility fU = new FilterUtility(getBaseContext());
//                fU.extractData(pharmacies);
//                LinkedList<String> types = fU.getTypes();
//                Vector<Integer> priceInterval = fU.getPriceInterval();
//                Vector<Integer> ratingInterval = fU.getRatingInterval();

//                getIntent().removeExtra("search_filters");
//
  //apotekeLekovi.add(new ApotekaLek(new Apoteka("apoteka"),new Lek("1","ime2","5"),"50"));


//
//
                if (!apotekeLekovi.isEmpty()) {
                    list_view.setAdapter(adapter);
                    list_view.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    //list_view.setOnItemClickListener(new ReservationClickListener(new ReservationsView()));

                    LatLng lastLatLng = new LatLng(0,0);
                    for (funky.pom16.funkyreservation.backend.data.ApotekaLek apLek : apotekeLekovi) {
                        String name = apLek.getApoteka().getName();

                    }
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "No matches found", Toast.LENGTH_SHORT);
                    toast.show();
                }
//
//                return false;
                sv.setIconified(false);

                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    return false;
                }
            });

    }

    public void onSearchViewClick(View v) {
        sv.setIconified(false);
    }

    // When back button on the android device is pressed, the app will go back to the main menu.
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent inMain = new Intent(this, MainActivity.class);
        inMain.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(inMain);
    }

    private void confirmReservation(int reservationID){
        ApotekaLek reservation = this.apotekeLekovi.get(reservationID);
        this.bService.confirmReservation(userName, reservation.getApoteka().getID());
        //this.pharmacies = this.bService.getReservations(userName);
        this.adapter.updateReservations(this.apotekeLekovi);
        this.selectionPosition = -1;
    }

    private void deleteReservation(int reservationID){
        ApotekaLek reservation = this.apotekeLekovi.get(reservationID);
        this.bService.cancelReservation(userName, reservation.getApoteka().getID());
        //this.pharmacies = this.bService.getReservations(userName);
        this.adapter.updateReservations(this.apotekeLekovi);
        this.selectionPosition = -1;
    }

    public boolean onSupportNavigateUp(){
        this.onBackPressed();
        return true;
    }

    public void onHomeClicked(View view){
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    class ReservationClickListener implements AdapterView.OnItemClickListener {
        private Context parent;
        public ReservationClickListener(Context parent){
            super();
            this.parent = parent;
        }
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectionPosition = position;

            AlertDialog.Builder resAccepted = new AlertDialog.Builder(this.parent);
            resAccepted.setTitle("Rezervacija leka");
            resAccepted.setMessage("Da li zelite da rezervisete lek?");
            resAccepted.setPositiveButton("Potvrdi", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    executeAjaxRequestReservation(apotekeLekovi.get(selectionPosition).getApoteka().getID(),apotekeLekovi.get(selectionPosition).getLek().getID(),1);
                }
            });
            Apoteka res = apotekeLekovi.get(position).getApoteka();

            resAccepted.setNeutralButton("Vrati se", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            });
            resAccepted.show();
        }
    }
    private void executeAjaxRequestSviLekovi(){
        String url = "http://10.0.2.2/projects/rs2/server.php"+"?akcija=sviLekovi";
        Log.v("url",url);


        AsyncHttpClient httpclient = new AsyncHttpClient();
        httpclient.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] h, byte[] b) {
                napraviListuLekova(new String(b));
                Log.i("TAG",new String(b));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }
    private void napraviListuLekova(String response) {
        /** Creating array adapter to set data in ListView using AJAX Data*/


        //ArrayList<Apoteka> apoteke = new ArrayList<Apoteka>();
        Log.v("response", response);
        String[] redLekova = response.split("::");
        ArrayList<String[]> koloneLekova = new ArrayList<String[]>();
        for (int i = 0; i < redLekova.length; i++) {
            koloneLekova.add(i, redLekova[i].split("\\.\\."));
            Log.v("koloneApoteka",koloneLekova.get(i)[0]);

            Lek lek = new Lek(koloneLekova.get(i)[0], koloneLekova.get(i)[1], koloneLekova.get(i)[2]);
            lekovi.add(lek);

        }
        Log.v("response", response);

    }
    private void executeAjaxRequestSveApotekeLek(){
        String url = "http://10.0.2.2/projects/rs2/server.php"+"?akcija=sveApotekeLek";
        Log.v("url",url);



        AsyncHttpClient httpclient = new AsyncHttpClient();
        httpclient.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] h, byte[] b) {
                napraviListuApoteka(new String(b));
                Log.i("TAG",new String(b));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }
    private void napraviListuApoteka(String response) {
        /** Creating array adapter to set data in ListView using AJAX Data*/


        //ArrayList<Apoteka> apoteke = new ArrayList<Apoteka>();
        Log.v("response", response);
        String[] redApotekaLek = response.split("::");
        ArrayList<String[]> koloneApotekaLek = new ArrayList<String[]>();
        for (int i = 0; i < redApotekaLek.length; i++) {
            koloneApotekaLek.add(i, redApotekaLek[i].split("\\.\\."));
            Log.v("koloneApoteka",koloneApotekaLek.get(i)[0]);

            ApotekaLek apLek = new ApotekaLek(new Apoteka(koloneApotekaLek.get(i)[0], koloneApotekaLek.get(i)[1], koloneApotekaLek.get(i)[2],koloneApotekaLek.get(i)[3], koloneApotekaLek.get(i)[4], koloneApotekaLek.get(i)[5], koloneApotekaLek.get(i)[6]), new Lek(koloneApotekaLek.get(i)[8],koloneApotekaLek.get(i)[9],koloneApotekaLek.get(i)[10]), koloneApotekaLek.get(i)[7]);
            apotekeLekoviSvi.add(apLek);

        }
        Log.v("response", response);

    }

    private void executeAjaxRequestReservation(int id_apoteke,int id_leka,int id_user){
        String url = "http://10.0.2.2/projects/rs2/server.php"+"?akcija=rezervacija&id_apoteka=" + id_apoteke+"&id_leka=" + id_leka +"&id_user="+id_user;
        Log.v("url",url);



        AsyncHttpClient httpclient = new AsyncHttpClient();
        httpclient.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] h, byte[] b) {
               // napraviListuApoteka(new String(b));
                Log.i("TAG",new String(b));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }
}