package funky.pom16.funkyreservation.backend.data;

import java.util.List;
import java.util.Vector;



public class Apoteka {
    private String name;
    private String adresa;
    private String vreme;
    private int ID;
    private String telefon;
    private String  website;

    private Vector<Double> location;
    private List<Lek> lekovi;

    public Apoteka(String id, String name, String adresa, String vreme, String koordinate, String telefon, String website){
        this.name = name;
        this.adresa = adresa;
        this.vreme = vreme;
        this.ID = Integer.parseInt(id);
        this.telefon = telefon;
        this.website = website;

        String[] temp = koordinate.split(", ");
        this.location = new Vector<Double>();
        this.location.add(Double.parseDouble(temp[0]));
        this.location.add(Double.parseDouble(temp[1]));
    }

    public Apoteka(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getAdresa() {
        return adresa;
    }

    public String getVreme() {
        return telefon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int id) {
        this.ID = id;
    }

//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public int getPriceCat() {
//        return priceCat;
//    }
//
//    public void setPriceCat(int priceCat) {
//        this.priceCat = priceCat;
//    }
//
//    public int getRating() {
//        return rating;
//    }
//
//    public void setRating(int rating) {
//        this.rating = rating;
//    }

    public Vector<Double> getLocation() {
        return location;
    }

    public void setLocation(Vector<Double> location) {
        this.location = location;
    }

//    public RestaurantDetails getDetails() {
//        return details;
//    }
//
//    public void setDetails(RestaurantDetails details) {
//        this.details = details;
//    }
}
