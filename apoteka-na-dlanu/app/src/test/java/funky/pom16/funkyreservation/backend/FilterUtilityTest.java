package funky.pom16.funkyreservation.backend;

import java.util.LinkedList;
import java.util.Vector;

import funky.pom16.funkyreservation.backend.data.Apoteka;
import funky.pom16.funkyreservation.backend.data.RestaurantDetails;


/**
 * Created by Sebastian Reichl on 08.06.2016.
 */
public class FilterUtilityTest {
    FilterUtility util;
    Apoteka newApoteka, newApoteka2;
    Vector<Double> coordinates = new Vector<>(2);
    Vector<Double> coordinates2 = new Vector<>(2);
    RestaurantDetails details;
    LinkedList<Apoteka> apoteke = new LinkedList<>();

    public FilterUtilityTest(){
        this.util = new FilterUtility(null);
         /* First restaurant*/
        newApoteka = new Apoteka("Italian Pizza");
        newApoteka.setType("italian");
        newApoteka.setPriceCat(5);
        newApoteka.setRating(5);
        coordinates.add(49.123456);
        coordinates.add(18.123456);
        details = new RestaurantDetails();
        details.setUrl("https://pizza.it");
        newApoteka.setDetails(details);
        newApoteka.setLocation(new Vector<>(coordinates));

        /* Second restaurant */
        newApoteka2 = new Apoteka("Indian food");
        newApoteka2.setType("indian");
        newApoteka2.setPriceCat(3);
        newApoteka2.setRating(3);
        coordinates2.add(49.2234567);
        coordinates2.add(18.2234567);
        details = new RestaurantDetails();
        details.setUrl("https://indian.food");
        newApoteka2.setDetails(details);
        newApoteka2.setLocation(new Vector<>(coordinates2));
    }
}
